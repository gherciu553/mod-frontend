const ROLocaleTranslations = require("./src/locale/ro");
const RULocaleTranslations = require("./src/locale/ru");

module.exports = {
  appName: "My App",
  /**
   * Public path thats the path where the server root are located
   * If the server run on localhost:3000 then "localhost:3000/" will be the public path
   *
   * Also it can be replaced by a CDN path
   * similar to WebPack publicPath
   */
  publicPath: "/",
  // localization
  mainLocale: "ro",
  allLocales: ["ro", "ru"],
  localesTranslations: {
    ro: ROLocaleTranslations,
    ru: RULocaleTranslations,
  },
  localesCompilationFoldersPaths: {
    // RO is the main one so it will be built in 2 folders in root one and in the named one
    ro: ["./public", "./public/ro"],
    ru: ["./public/ru"],
  },
};
