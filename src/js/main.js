//=require ./vendors/jquery.js
//=require ./vendors/popper.js
//=require ./vendors/bootstrap.js
//=require ./vendors/vide.js
//=require ./vendors/slick.js
//=require ./vendors/wow.js
//=require ./vendors/fancybox.js

$(function () {
  //video header
  $(".header-video").vide(
    {
      mp4: "/assets/hero-video/Fade-To-Black.mp4",
      webm: "/assets/hero-video/Fade-To-Black.webm",
      ogv: "/assets/hero-video/Fade-To-Black.ogv",
      poster: "/assets/hero-video/Fade-To-Black.jpg",
    },
    {
      muted: true,
      loop: true,
      autoplay: true,
      position: "50% 50%", // Similar to the CSS `background-position` property.
      posterType: "detect", // Poster image type. "detect" — auto-detection; "none" — no poster; "jpg", "png", "gif",... - extensions.
      resizing: true, // Auto-resizing, read: https://github.com/VodkaBears/Vide#resizing
      bgColor: "transparent", // Allow custom background-color for Vide div,
    }
  );
  //map on click close toggle d-none
  $(".close-map-contact1").click(function () {
    $(".map-modal1").toggleClass("d-none");
  });
  $(".close-map-contact2").click(function () {
    $(".map-modal2").toggleClass("d-none");
  });
  //navbar event on scroll
  $(document).on("scroll", function () {
    if (
      $(document).scrollTop() > $(".navbar").height() &&
      $(".navbar").hasClass("navbar-in-scroll") != true
    ) {
      $(".navbar").addClass("navbar-in-scroll");
      if ($(window).width() > 564) {
        $(".navbar-on-hidden").css({ height: "120px" });
      } else {
        $(".navbar-on-hidden").css({ height: "75px" });
      }
    } else {
      if ($(document).scrollTop() < $(".navbar").height()) {
        $(".navbar").removeClass("navbar-in-scroll");
        $(".navbar-on-hidden").css({ height: "0px" });
      }
    }
  });

  // ch navbar icon toggler
  var isClosed = false;
  $("#menu-icon").on("click", function () {
    $(".navbar").toggleClass("navbar-in-click");
    if (!isClosed) {
      isClosed = true;
      $("#menu-icon-img").attr("src", "/assets/close.png");
    } else {
      isClosed = false;
      $("#menu-icon-img").attr("src", "/assets/menu.png");
    }
  });
  //ch bg of nav item on hover menu toggle
  $(".menu-toggle").hover(function () {
    if ($(window).width() > 1190) {
      $(this).parent().toggleClass("bg-white");
      $(this).parent().children(".nav-link").toggleClass("text-dark");
    }
  });
  // img close langHover
  var imgCloseHover = false;
  $(".close").hover(function () {
    if (imgCloseHover) {
      imgCloseHover = false;
      $(this).attr("src", "/assets/close.png");
    } else {
      imgCloseHover = true;
      $(this).attr("src", "/assets/close_hover.png");
    }
  });
  //show menu toggle on sm
  if ($(window).width() < 1205) {
    $(".nav-item").on("click", function (e) {
      $(this).children(".menu-toggle").toggleClass("d-none");
    });
  }
  //init animations
  new WOW().init();
  //init slick slider
  if ($(".slider-component").length > 0) {
    $(".slider-component").slick({
      dots: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1000,
      infinite: true,
      adaptiveHeight: false,
      responsive: false,
      variableWidth: true,
      prevArrow:
        "<div class='d-flex align-items-center justify-content-center slider-component-controls' style='cursor:pointer;left:20px;top:40%;width:60px;height:60px;background:#EE3648;position:absolute;z-index:888;border-radius:50%'><img src='/assets/arrowPrevSlider.png'/></div>",
      nextArrow:
        "<div class='d-flex align-items-center justify-content-center slider-component-controls' style='cursor:pointer;right:18px;top:40%;width:60px;height:60px;background:#EE3648;position:absolute;z-index:888;border-radius:50%'><img src='/assets/arrowNextSlider.png'/></div>",
    });
  }
  //init slick slider for content menu
  if ($(".my-secondary-menu-slider").length > 0) {
    $(".my-secondary-menu-slider").slick({
      dots: false,
      slidesToShow: 7,
      slidesToScroll: 1,
      adaptiveHeight: false,
      responsive: false,
      infinite: false,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }

  //init slick slider for Areas
  if ($(".my-areas-slider").length > 0) {
    $(".my-areas-slider").slick({
      dots: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      adaptiveHeight: false,
      responsive: false,
      variableWidth: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }
  //init slick slider for news
  if ($(window).width() < 764) {
    $(".index-news-slider").slick({
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: false,
      adaptiveHeight: false,
      responsive: false,
      variableWidth: true,
    });
    $(".m-new").first().unwrap();
  }
  //init listening
  $(".input-file").on("change", function () {
    $(".input-file-name").text($(this).prop("files")[0].name);
  });
  //togle footer
  $(".toggle-footer").on("click", function () {
    $(this)
      .parent()
      .children(".menu-to-toggle")
      .toggleClass("items-footer-toggle")
      .toggleClass("d-none");
    if ($(this).children("small").text() == "-") {
      $(this).children("small").text("+");
    } else {
      $(this).children("small").text("-");
    }
  });
  //scroll btn
  $(".scroll-btn").click(function () {
    $("html,body").animate(
      { scrollTop: $("#about-section").offset().top - 120 },
      1000
    );
  });
    
  //content links

  $(".content-links").click(function () {
    $(".content-links").each(function (e) {
      if ($(this).hasClass("font-weight-bold")) {
        $(this).removeClass("font-weight-bold");
      }
    });

    $(this).toggleClass("font-weight-bold");

    $(".tab").each(function (e) {
      if (!$(this).hasClass("d-none")) {
        $(this).addClass("d-none");
      }
    });

    $("#" + $(this).data("tabid")).toggleClass("d-none");
  });
});
