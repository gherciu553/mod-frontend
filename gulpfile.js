const gulp = require("gulp");
const browserSync = require("browser-sync").create();
const stylus = require("gulp-stylus");
const rename = require("gulp-rename");
const nunjucks = require("gulp-nunjucks");
const include = require("gulp-include");
const autoprefixer = require("gulp-autoprefixer");
const cssmin = require("gulp-cssmin");
const mergeStream = require("merge-stream");
const config = require("./config");

// Compile stylus into CSS & auto-inject into browsers
gulp.task("compile-styl", function () {
  return (
    gulp
      // Including only main Styl as entrypoint
      .src("./src/styl/main.styl")
      .pipe(
        stylus({
          "include css": true,
        })
      )
      .pipe(
        autoprefixer({
          cascade: false,
        })
      )
      .pipe(cssmin())
      .pipe(gulp.dest("./public/css"))
  );
});

gulp.task("compile-js", () =>
  gulp
    // Including only main JS as entrypoint
    .src("./src/js/main.js")
    .pipe(include())
    .pipe(gulp.dest("./public/js"))
);

gulp.task("compile-assets", () =>
  gulp
    // Including all assets as is
    .src("./src/assets/**/*")
    .pipe(gulp.dest("./public/assets"))
);

gulp.task("compile-njk", () => {
  // Creating stream for each locale to have these compiled with custom translations and data in separate folders
  const localesStreams = Object.entries(
    config.localesCompilationFoldersPaths
  ).map(([locale, compilationPaths]) => {
    const currentLocaleData = {
      config: {
        // Making the entire config available in all njk files during the compilation
        ...config,
        // Adding also Current locale to have info in the files which locale is currently active
        currentLocale: locale,
      },
      // Translations for current locale
      t: config.localesTranslations[locale],
    };

    return compilationPaths.map((compilationPath) =>
      gulp
        .src("./src/njk/*.njk")
        .pipe(nunjucks.compile(currentLocaleData))
        .pipe(
          rename({
            extname: ".html",
          })
        )
        .pipe(gulp.dest(compilationPath))
    );
  });

  return mergeStream(
    localesStreams
      // Using flat because locales streams is and array of arrays
      .flat()
  );
});

// development task
gulp.task(
  "development",
  ["compile-styl", "compile-js", "compile-assets", "compile-njk"],
  function () {
    // Watching for changes to compile
    gulp.watch("src/styl/**/*.styl", ["compile-styl"]);
    gulp.watch("src/js/**/*.js", ["compile-js"]);
    gulp.watch("src/js/**/*", ["compile-assets"]);
    gulp.watch("src/njk/**/*.njk", ["compile-njk"]);

    // Watching public folder which is served by the server for changes to reload
    gulp.watch("public/**/*").on("change", browserSync.reload);
    // Starting browsersync server
    browserSync.init({
      server: "./public",
      port: 3000,
    });
  }
);

// Prod task
gulp.task("production", [
  "compile-styl",
  "compile-js",
  "compile-assets",
  "compile-njk",
]);

gulp.task("default", ["development"]);
